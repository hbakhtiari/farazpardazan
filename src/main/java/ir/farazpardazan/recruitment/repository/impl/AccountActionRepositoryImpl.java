package ir.farazpardazan.recruitment.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.querydsl.core.BooleanBuilder;

import ir.farazpardazan.recruitment.domain.AccountAction;
import ir.farazpardazan.recruitment.domain.QAccountAction;
import ir.farazpardazan.recruitment.repository.AccountActionRepository;

public class AccountActionRepositoryImpl implements AccountActionRepositoryCustom {
	
	@Autowired
	private AccountActionRepository accountActionRepository;
	
	@Override
	public Page<AccountAction> findAllForBankAccount(Pageable pageable, Long bankAccountId) {
		QAccountAction accountAction = QAccountAction.accountAction;
		
		BooleanBuilder predicateBuilder = new BooleanBuilder();
		
		if (bankAccountId != null)
			predicateBuilder
					.and(
							accountAction.source.id.eq(bankAccountId)
						.or(
							accountAction.destination.id.eq(bankAccountId)
						)
					);
		return accountActionRepository.findAll(predicateBuilder, pageable);
	}

}
