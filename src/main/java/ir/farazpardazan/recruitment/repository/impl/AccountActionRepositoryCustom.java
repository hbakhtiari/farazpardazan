package ir.farazpardazan.recruitment.repository.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ir.farazpardazan.recruitment.domain.AccountAction;

public interface AccountActionRepositoryCustom {

	Page<AccountAction> findAllForBankAccount(Pageable pageable, Long bankAccountId);
}
