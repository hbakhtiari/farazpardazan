package ir.farazpardazan.recruitment.repository;

import ir.farazpardazan.recruitment.domain.BankUser;
import ir.farazpardazan.recruitment.repository.impl.BankUserRepositoryCustom;

import org.springframework.data.jpa.repository.*;
//import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BankUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankUserRepository extends JpaRepository<BankUser, Long>, BankUserRepositoryCustom {

}
