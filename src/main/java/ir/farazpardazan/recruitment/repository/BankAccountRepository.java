package ir.farazpardazan.recruitment.repository;

import ir.farazpardazan.recruitment.domain.BankAccount;

import java.util.Optional;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@Query("from BankAccount a where a.id = :id")
	Optional<BankAccount> findByIdForUpdate(Long id);

}
