package ir.farazpardazan.recruitment.repository;

import ir.farazpardazan.recruitment.domain.AccountAction;
import ir.farazpardazan.recruitment.repository.impl.AccountActionRepositoryCustom;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AccountAction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccountActionRepository extends JpaRepository<AccountAction, Long>, QuerydslPredicateExecutor<AccountAction>, AccountActionRepositoryCustom {

}
