package ir.farazpardazan.recruitment.domain.enumeration;

/**
 * The AccountActionType enumeration.
 */
public enum AccountActionType {
    TRANSFER, PAYMENT, CHARGE
}
