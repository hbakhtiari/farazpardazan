package ir.farazpardazan.recruitment.domain.enumeration;

/**
 * The CurrencyType enumeration.
 */
public enum CurrencyType {
    DOLLAR, RIAL
}
