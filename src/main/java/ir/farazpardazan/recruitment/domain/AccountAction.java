package ir.farazpardazan.recruitment.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import ir.farazpardazan.recruitment.domain.enumeration.AccountActionType;

import ir.farazpardazan.recruitment.domain.enumeration.CurrencyType;

/**
 * A AccountAction.
 */
@Entity
@Table(name = "account_action")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AccountAction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "action_type")
    private AccountActionType actionType;

    @Column(name = "ammount")
    private Double ammount;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency_type")
    private CurrencyType currencyType;

    @ManyToOne
    @JsonIgnoreProperties("")
    private BankAccount source;

    @ManyToOne
    @JsonIgnoreProperties("")
    private BankAccount destination;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountActionType getActionType() {
        return actionType;
    }

    public AccountAction actionType(AccountActionType actionType) {
        this.actionType = actionType;
        return this;
    }

    public void setActionType(AccountActionType actionType) {
        this.actionType = actionType;
    }

    public Double getAmmount() {
        return ammount;
    }

    public AccountAction ammount(Double ammount) {
        this.ammount = ammount;
        return this;
    }

    public void setAmmount(Double ammount) {
        this.ammount = ammount;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public AccountAction currencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
        return this;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    public BankAccount getSource() {
        return source;
    }

    public AccountAction source(BankAccount bankAccount) {
        this.source = bankAccount;
        return this;
    }

    public void setSource(BankAccount bankAccount) {
        this.source = bankAccount;
    }

    public BankAccount getDestination() {
        return destination;
    }

    public AccountAction destination(BankAccount bankAccount) {
        this.destination = bankAccount;
        return this;
    }

    public void setDestination(BankAccount bankAccount) {
        this.destination = bankAccount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountAction accountAction = (AccountAction) o;
        if (accountAction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accountAction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccountAction{" +
            "id=" + getId() +
            ", actionType='" + getActionType() + "'" +
            ", ammount=" + getAmmount() +
            ", currencyType='" + getCurrencyType() + "'" +
            "}";
    }
}
