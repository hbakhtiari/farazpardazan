package ir.farazpardazan.recruitment.web.rest;

import com.codahale.metrics.annotation.Timed;
import ir.farazpardazan.recruitment.service.BankUserService;
import ir.farazpardazan.recruitment.web.rest.errors.BadRequestAlertException;
import ir.farazpardazan.recruitment.web.rest.util.HeaderUtil;
import ir.farazpardazan.recruitment.web.rest.util.PaginationUtil;
import ir.farazpardazan.recruitment.service.dto.BankUserDTO;
import ir.farazpardazan.recruitment.service.dto.BankUserUpdateDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BankUser.
 */
@RestController
@RequestMapping("/api")
public class BankUserResource {

    private final Logger log = LoggerFactory.getLogger(BankUserResource.class);

    private static final String ENTITY_NAME = "recruitmentTestBankUser";

    private final BankUserService bankUserService;

    public BankUserResource(BankUserService bankUserService) {
        this.bankUserService = bankUserService;
    }

    /**
     * POST  /bank-users : Create a new bankUser.
     *
     * @param bankUserDTO the bankUserDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bankUserDTO, or with status 400 (Bad Request) if the bankUser has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bank-users")
    @Timed
    public ResponseEntity<BankUserDTO> createBankUser(@Valid @RequestBody BankUserDTO bankUserDTO) throws URISyntaxException {
        log.debug("REST request to save BankUser : {}", bankUserDTO);
        if (bankUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new bankUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BankUserDTO result = bankUserService.save(bankUserDTO);
        return ResponseEntity.created(new URI("/api/bank-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bank-users : Updates an existing bankUser.
     *
     * @param bankUserUpdateDTO the bankUserDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bankUserDTO,
     * or with status 400 (Bad Request) if the bankUserDTO is not valid,
     * or with status 500 (Internal Server Error) if the bankUserDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bank-users")
    @Timed
    public ResponseEntity<BankUserDTO> updateBankUser(@Valid @RequestBody BankUserUpdateDTO bankUserUpdateDTO) throws URISyntaxException {
        log.debug("REST request to update BankUser : {}", bankUserUpdateDTO);
        if (bankUserUpdateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BankUserDTO result = bankUserService.update(bankUserUpdateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bankUserUpdateDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bank-users : get all the bankUsers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bankUsers in body
     */
    @GetMapping("/bank-users")
    @Timed
    public ResponseEntity<List<BankUserDTO>> getAllBankUsers(Pageable pageable) {
        log.debug("REST request to get a page of BankUsers");
        Page<BankUserDTO> page = bankUserService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bank-users");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /bank-users/:id : get the "id" bankUser.
     *
     * @param id the id of the bankUserDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bankUserDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bank-users/{id}")
    @Timed
    public ResponseEntity<BankUserDTO> getBankUser(@PathVariable Long id) {
        log.debug("REST request to get BankUser : {}", id);
        Optional<BankUserDTO> bankUserDTO = bankUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bankUserDTO);
    }

//    /**
//     * DELETE  /bank-users/:id : delete the "id" bankUser.
//     *
//     * @param id the id of the bankUserDTO to delete
//     * @return the ResponseEntity with status 200 (OK)
//     */
//    @DeleteMapping("/bank-users/{id}")
//    @Timed
//    public ResponseEntity<Void> deleteBankUser(@PathVariable Long id) {
//        log.debug("REST request to delete BankUser : {}", id);
//        bankUserService.delete(id);
//        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
//    }
}
