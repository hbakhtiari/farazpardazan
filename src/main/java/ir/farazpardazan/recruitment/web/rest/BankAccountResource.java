package ir.farazpardazan.recruitment.web.rest;

import com.codahale.metrics.annotation.Timed;

import ir.farazpardazan.recruitment.domain.BankAccount;
import ir.farazpardazan.recruitment.service.BankAccountService;
import ir.farazpardazan.recruitment.web.rest.errors.BadRequestAlertException;
import ir.farazpardazan.recruitment.web.rest.util.HeaderUtil;
import ir.farazpardazan.recruitment.web.rest.util.PaginationUtil;
import ir.farazpardazan.recruitment.service.dto.BankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.ChargeBankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.PayByBankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.TransferDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BankAccount.
 */
@RestController
@RequestMapping("/api")
public class BankAccountResource {

    private final Logger log = LoggerFactory.getLogger(BankAccountResource.class);

    private static final String ENTITY_NAME = "recruitmentTestBankAccount";

    private final BankAccountService bankAccountService;

    public BankAccountResource(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    /**
     * POST  /bank-accounts : Create a new bankAccount.
     *
     * @param bankAccountDTO the bankAccountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bankAccountDTO, or with status 400 (Bad Request) if the bankAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bank-accounts")
    @Timed
    public ResponseEntity<BankAccountDTO> createBankAccount(@RequestBody BankAccountDTO bankAccountDTO) throws URISyntaxException {
        log.debug("REST request to save BankAccount : {}", bankAccountDTO);
        if (bankAccountDTO.getId() != null) {
            throw new BadRequestAlertException("A new bankAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BankAccountDTO result = bankAccountService.save(bankAccountDTO);
        return ResponseEntity.created(new URI("/api/bank-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    
    
    @PutMapping("/bank-accounts/pay")
    @Timed
    public ResponseEntity<BankAccountDTO> tryPayByAccount(@RequestBody PayByBankAccountDTO payByBankAccountDTO) {
        log.debug("REST request to pay by BankAccount : {}", payByBankAccountDTO);
        
        if(payByBankAccountDTO.getBankAccountId() == null)
        	throw new BadRequestAlertException("bankAccountId must not be null in payment", ENTITY_NAME, "BankAccount_payment_bankAccountId_null");
        
        if(payByBankAccountDTO.getAmmount() == null)
        	throw new BadRequestAlertException("ammount must not be null in payment", ENTITY_NAME, "BankAccount_payment_ammount_null");
        
        if(payByBankAccountDTO.getAmmount() <= 0)
        	throw new BadRequestAlertException("ammount must be positive in payment", ENTITY_NAME, "BankAccount_payment_ammount_not_positive");
        
        if(payByBankAccountDTO.getCurrencyType() == null)
        	throw new BadRequestAlertException("currencyType must be valid in payment", ENTITY_NAME, "BankAccount_payment_currencyType_invalid");
    	
		BankAccountDTO result = bankAccountService.payByBankAccount(payByBankAccountDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, payByBankAccountDTO.getBankAccountId().toString()))
				.body(result);
	}
    
    @PutMapping("/bank-accounts/charge")
    @Timed
    public ResponseEntity<BankAccountDTO> chargeAccount(@RequestBody ChargeBankAccountDTO chargeBankAccountDTO) {
        log.debug("REST request to charge BankAccount : {}", chargeBankAccountDTO);
        
        if(chargeBankAccountDTO.getBankAccountId() == null)
        	throw new BadRequestAlertException("bankAccountId must not be null in charging", ENTITY_NAME, "BankAccount_charging_bankAccountId_null");
        
        if(chargeBankAccountDTO.getAmmount() == null)
        	throw new BadRequestAlertException("ammount must not be null in charging", ENTITY_NAME, "BankAccount_charging_ammount_null");
        
        if(chargeBankAccountDTO.getAmmount() <= 0)
        	throw new BadRequestAlertException("ammount must be positive in charging", ENTITY_NAME, "BankAccount_charging_ammount_not_positive");
        
        if(chargeBankAccountDTO.getCurrencyType() == null)
        	throw new BadRequestAlertException("currencyType must be valid in charging", ENTITY_NAME, "BankAccount_charging_currencyType_invalid");
        
		BankAccountDTO result = bankAccountService.chargeBankAccount(chargeBankAccountDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chargeBankAccountDTO.getBankAccountId().toString()))
				.body(result);
    }
    
    @PutMapping("/bank-accounts/transfer")
    @Timed
    public ResponseEntity<List<BankAccountDTO>> tryTransfer(@RequestBody TransferDTO transferDTO) {

    	if(transferDTO.getSourceId() == null)
        	throw new BadRequestAlertException("sourceId must not be null in transfer", ENTITY_NAME, "BankAccount_transfer_sourceId_null");
    	
    	if(transferDTO.getDestinationId() == null)
        	throw new BadRequestAlertException("destinationId must not be null in transfer", ENTITY_NAME, "BankAccount_transfer_destinationId_null");
       
    	if(transferDTO.getSourceId() == transferDTO.getDestinationId())
        	throw new BadRequestAlertException("sourceId and destinationId must not be identical in transfer", ENTITY_NAME, "BankAccount_transfer_sourceId_destinationId_identical");
    	
        if(transferDTO.getAmmount() == null)
        	throw new BadRequestAlertException("ammount must not be null in transfer", ENTITY_NAME, "BankAccount_transfer_ammount_null");
        
        if(transferDTO.getAmmount() <= 0)
        	throw new BadRequestAlertException("ammount must be positive in transfer", ENTITY_NAME, "BankAccount_transfer_ammount_not_positive");
        
        if(transferDTO.getCurrencyType() == null)
        	throw new BadRequestAlertException("currencyType must be valid in transfer", ENTITY_NAME, "BankAccount_transfer_currencyType_invalid");
		List<BankAccountDTO> result = bankAccountService.tryTransfer(transferDTO);
		return ResponseEntity.ok()
				.body(result);
    }
    
    

//    /**
//     * PUT  /bank-accounts : Updates an existing bankAccount.
//     *
//     * @param bankAccountDTO the bankAccountDTO to update
//     * @return the ResponseEntity with status 200 (OK) and with body the updated bankAccountDTO,
//     * or with status 400 (Bad Request) if the bankAccountDTO is not valid,
//     * or with status 500 (Internal Server Error) if the bankAccountDTO couldn't be updated
//     * @throws URISyntaxException if the Location URI syntax is incorrect
//     */
//    @PutMapping("/bank-accounts")
//    @Timed
//    public ResponseEntity<BankAccountDTO> updateBankAccount(@RequestBody BankAccountDTO bankAccountDTO) throws URISyntaxException {
//        log.debug("REST request to update BankAccount : {}", bankAccountDTO);
//        if (bankAccountDTO.getId() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        BankAccountDTO result = bankAccountService.save(bankAccountDTO);
//        return ResponseEntity.ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bankAccountDTO.getId().toString()))
//            .body(result);
//    }

    /**
     * GET  /bank-accounts : get all the bankAccounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bankAccounts in body
     */
    @GetMapping("/bank-accounts")
    @Timed
    public ResponseEntity<List<BankAccountDTO>> getAllBankAccounts(Pageable pageable) {
        log.debug("REST request to get a page of BankAccounts");
        Page<BankAccountDTO> page = bankAccountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bank-accounts");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /bank-accounts/:id : get the "id" bankAccount.
     *
     * @param id the id of the bankAccountDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bankAccountDTO, or with status 404 (Not Found)
     */
    @GetMapping("/bank-accounts/{id}")
    @Timed
    public ResponseEntity<BankAccountDTO> getBankAccount(@PathVariable Long id) {
        log.debug("REST request to get BankAccount : {}", id);
        Optional<BankAccountDTO> bankAccountDTO = bankAccountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bankAccountDTO);
    }

//    /**
//     * DELETE  /bank-accounts/:id : delete the "id" bankAccount.
//     *
//     * @param id the id of the bankAccountDTO to delete
//     * @return the ResponseEntity with status 200 (OK)
//     */
//    @DeleteMapping("/bank-accounts/{id}")
//    @Timed
//    public ResponseEntity<Void> deleteBankAccount(@PathVariable Long id) {
//        log.debug("REST request to delete BankAccount : {}", id);
//        bankAccountService.delete(id);
//        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
//    }
}
