package ir.farazpardazan.recruitment.web.rest;

import com.codahale.metrics.annotation.Timed;
import ir.farazpardazan.recruitment.service.AccountActionService;
import ir.farazpardazan.recruitment.web.rest.errors.BadRequestAlertException;
import ir.farazpardazan.recruitment.web.rest.util.HeaderUtil;
import ir.farazpardazan.recruitment.web.rest.util.PaginationUtil;
import ir.farazpardazan.recruitment.service.dto.AccountActionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AccountAction.
 */
@RestController
@RequestMapping("/api")
public class AccountActionResource {

    private final Logger log = LoggerFactory.getLogger(AccountActionResource.class);

    private static final String ENTITY_NAME = "recruitmentTestAccountAction";

    private final AccountActionService accountActionService;

    public AccountActionResource(AccountActionService accountActionService) {
        this.accountActionService = accountActionService;
    }

//    /**
//     * POST  /account-actions : Create a new accountAction.
//     *
//     * @param accountActionDTO the accountActionDTO to create
//     * @return the ResponseEntity with status 201 (Created) and with body the new accountActionDTO, or with status 400 (Bad Request) if the accountAction has already an ID
//     * @throws URISyntaxException if the Location URI syntax is incorrect
//     */
//    @PostMapping("/account-actions")
//    @Timed
//    public ResponseEntity<AccountActionDTO> createAccountAction(@RequestBody AccountActionDTO accountActionDTO) throws URISyntaxException {
//        log.debug("REST request to save AccountAction : {}", accountActionDTO);
//        if (accountActionDTO.getId() != null) {
//            throw new BadRequestAlertException("A new accountAction cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        AccountActionDTO result = accountActionService.save(accountActionDTO);
//        return ResponseEntity.created(new URI("/api/account-actions/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }

//    /**
//     * PUT  /account-actions : Updates an existing accountAction.
//     *
//     * @param accountActionDTO the accountActionDTO to update
//     * @return the ResponseEntity with status 200 (OK) and with body the updated accountActionDTO,
//     * or with status 400 (Bad Request) if the accountActionDTO is not valid,
//     * or with status 500 (Internal Server Error) if the accountActionDTO couldn't be updated
//     * @throws URISyntaxException if the Location URI syntax is incorrect
//     */
//    @PutMapping("/account-actions")
//    @Timed
//    public ResponseEntity<AccountActionDTO> updateAccountAction(@RequestBody AccountActionDTO accountActionDTO) throws URISyntaxException {
//        log.debug("REST request to update AccountAction : {}", accountActionDTO);
//        if (accountActionDTO.getId() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        AccountActionDTO result = accountActionService.save(accountActionDTO);
//        return ResponseEntity.ok()
//            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, accountActionDTO.getId().toString()))
//            .body(result);
//    }

//    /**
//     * GET  /account-actions : get all the accountActions.
//     *
//     * @param pageable the pagination information
//     * @return the ResponseEntity with status 200 (OK) and the list of accountActions in body
//     */
//    @GetMapping("/account-actions")
//    @Timed
//    public ResponseEntity<List<AccountActionDTO>> getAllAccountActions(Pageable pageable) {
//        log.debug("REST request to get a page of AccountActions");
//        Page<AccountActionDTO> page = accountActionService.findAll(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/account-actions");
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }
    
    /**
     * GET  /account-actions : get all the accountActions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of accountActions in body
     */
    @GetMapping("/account-actions")
    @Timed
    public ResponseEntity<List<AccountActionDTO>> getAllAccountActionsForBankAccount(Pageable pageable, @RequestParam(name="bankAccountId", required=false) Long bankAccountId) {
        log.debug("REST request to get a page of AccountActions");
        Page<AccountActionDTO> page = accountActionService.findAllForBankAccount(pageable, bankAccountId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/account-actions");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /account-actions/:id : get the "id" accountAction.
     *
     * @param id the id of the accountActionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accountActionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/account-actions/{id}")
    @Timed
    public ResponseEntity<AccountActionDTO> getAccountAction(@PathVariable Long id) {
        log.debug("REST request to get AccountAction : {}", id);
        Optional<AccountActionDTO> accountActionDTO = accountActionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(accountActionDTO);
    }

//    /**
//     * DELETE  /account-actions/:id : delete the "id" accountAction.
//     *
//     * @param id the id of the accountActionDTO to delete
//     * @return the ResponseEntity with status 200 (OK)
//     */
//    @DeleteMapping("/account-actions/{id}")
//    @Timed
//    public ResponseEntity<Void> deleteAccountAction(@PathVariable Long id) {
//        log.debug("REST request to delete AccountAction : {}", id);
//        accountActionService.delete(id);
//        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
//    }
}
