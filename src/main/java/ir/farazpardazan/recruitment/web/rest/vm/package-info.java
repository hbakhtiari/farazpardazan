/**
 * View Models used by Spring MVC REST controllers.
 */
package ir.farazpardazan.recruitment.web.rest.vm;
