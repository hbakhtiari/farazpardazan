package ir.farazpardazan.recruitment.service.mapper;

import ir.farazpardazan.recruitment.domain.*;
import ir.farazpardazan.recruitment.service.dto.BankAccountDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity BankAccount and its DTO BankAccountDTO.
 */
@Mapper(componentModel = "spring", uses = {BankUserMapper.class})
public interface BankAccountMapper extends EntityMapper<BankAccountDTO, BankAccount> {

    @Mapping(source = "bankUser.id", target = "bankUserId")
    BankAccountDTO toDto(BankAccount bankAccount);

    @Mapping(source = "bankUserId", target = "bankUser")
    BankAccount toEntity(BankAccountDTO bankAccountDTO);

    default BankAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankAccount bankAccount = new BankAccount();
        bankAccount.setId(id);
        return bankAccount;
    }
}
