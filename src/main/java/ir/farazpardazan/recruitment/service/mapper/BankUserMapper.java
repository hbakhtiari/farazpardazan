package ir.farazpardazan.recruitment.service.mapper;

import ir.farazpardazan.recruitment.domain.*;
import ir.farazpardazan.recruitment.service.dto.BankUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity BankUser and its DTO BankUserDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BankUserMapper extends EntityMapper<BankUserDTO, BankUser> {



    default BankUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        BankUser bankUser = new BankUser();
        bankUser.setId(id);
        return bankUser;
    }
}
