package ir.farazpardazan.recruitment.service.mapper;

import ir.farazpardazan.recruitment.domain.*;
import ir.farazpardazan.recruitment.service.dto.AccountActionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AccountAction and its DTO AccountActionDTO.
 */
@Mapper(componentModel = "spring", uses = {BankAccountMapper.class})
public interface AccountActionMapper extends EntityMapper<AccountActionDTO, AccountAction> {

    @Mapping(source = "source.id", target = "sourceId")
    @Mapping(source = "destination.id", target = "destinationId")
    AccountActionDTO toDto(AccountAction accountAction);

    @Mapping(source = "sourceId", target = "source")
    @Mapping(source = "destinationId", target = "destination")
    AccountAction toEntity(AccountActionDTO accountActionDTO);

    default AccountAction fromId(Long id) {
        if (id == null) {
            return null;
        }
        AccountAction accountAction = new AccountAction();
        accountAction.setId(id);
        return accountAction;
    }
}
