package ir.farazpardazan.recruitment.service.impl;

import ir.farazpardazan.recruitment.service.BankAccountService;
import ir.farazpardazan.recruitment.service.CurrencyExchangeService;
import ir.farazpardazan.recruitment.domain.AccountAction;
import ir.farazpardazan.recruitment.domain.BankAccount;
import ir.farazpardazan.recruitment.domain.enumeration.AccountActionType;
import ir.farazpardazan.recruitment.domain.enumeration.CurrencyType;
import ir.farazpardazan.recruitment.repository.AccountActionRepository;
import ir.farazpardazan.recruitment.repository.BankAccountRepository;
import ir.farazpardazan.recruitment.service.dto.BankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.ChargeBankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.PayByBankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.TransferDTO;
import ir.farazpardazan.recruitment.service.mapper.BankAccountMapper;
import ir.farazpardazan.recruitment.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.auditing.CurrentDateTimeProvider;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing BankAccount.
 */
@Service
@Transactional
public class BankAccountServiceImpl implements BankAccountService {
	
    private static final String ENTITY_NAME = "recruitmentTestBankAccount";

    private final Logger log = LoggerFactory.getLogger(BankAccountServiceImpl.class);

    private final BankAccountRepository bankAccountRepository;

    private final BankAccountMapper bankAccountMapper;
    
    private final AccountActionRepository accountActionRepository;
    
    private final CurrencyExchangeService currencyExchangeService;

    public BankAccountServiceImpl(BankAccountRepository bankAccountRepository, BankAccountMapper bankAccountMapper, CurrencyExchangeService currencyExchangeService, AccountActionRepository accountActionRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.bankAccountMapper = bankAccountMapper;
        this.currencyExchangeService = currencyExchangeService;
        this.accountActionRepository = accountActionRepository;
    }

    /**
     * Save a bankAccount.
     *
     * @param bankAccountDTO the entity to save
     * @return the persisted entity
     */
    @Override
    @Transactional
    public BankAccountDTO save(BankAccountDTO bankAccountDTO) {
        log.debug("Request to save BankAccount : {}", bankAccountDTO);
        
        BankAccount bankAccount = bankAccountMapper.toEntity(bankAccountDTO);
        bankAccount = bankAccountRepository.save(bankAccount);
        return bankAccountMapper.toDto(bankAccount);
    }
    
    /**
     * Pay by bankAccount.
     *
     * @param payByBankAccountDTO as payment details
     * @return update bankAccount
     */
    @Override
    public BankAccountDTO payByBankAccount(PayByBankAccountDTO payByBankAccountDTO ) {
        log.debug("Request to pay by BankAccount : {}", payByBankAccountDTO);
        Optional<BankAccount> sourceOptional = bankAccountRepository.findByIdForUpdate(payByBankAccountDTO.getBankAccountId());
        if(sourceOptional.isPresent() == false)
        	throw new BadRequestAlertException("Bank account doesn't exist to pay", ENTITY_NAME, "BankAccount_payment_bankAccountId_not_exists");
        
        BankAccount source = sourceOptional.get();
        Double reducedBalance = this.computeReduction(source, payByBankAccountDTO.getAmmount(), payByBankAccountDTO.getCurrencyType());
        if( reducedBalance >= 0)
        	source.setBalance(reducedBalance);
        else
        	throw new BadRequestAlertException("Insufficient fund to pay", ENTITY_NAME, "BankAccount_payment_insufficient_fund");
        	
        source = bankAccountRepository.save(source);
        
        AccountAction paymentAction = new AccountAction();
        paymentAction.setActionType(AccountActionType.PAYMENT);
        paymentAction.setAmmount(payByBankAccountDTO.getAmmount());
        paymentAction.setCurrencyType(payByBankAccountDTO.getCurrencyType());
        paymentAction.setSource(source);
        
        accountActionRepository.save(paymentAction);
        
        return bankAccountMapper.toDto(source);
    }
    
    /**
     * charge bankAccount.
     *
     * @param chargeBankAccountDTO as charging details
     * @return update bankAccount
     */
    @Override
    public BankAccountDTO chargeBankAccount(ChargeBankAccountDTO chargeBankAccountDTO) {
        log.debug("Request to charge BankAccount : {}", chargeBankAccountDTO);
        Optional<BankAccount> sourceOptional = bankAccountRepository.findByIdForUpdate(chargeBankAccountDTO.getBankAccountId());
        if(sourceOptional.isPresent() == false)
        	throw new BadRequestAlertException("Bank account doesn't exist to charge", ENTITY_NAME, "BankAccount_charge_bankAccountId_not_exists");
        
        BankAccount source = sourceOptional.get();
		source.setBalance(this.computeAddition(source, chargeBankAccountDTO.getAmmount(),
				chargeBankAccountDTO.getCurrencyType()));
		source = bankAccountRepository.save(source);
        
        AccountAction chargeAction = new AccountAction();
        chargeAction.setActionType(AccountActionType.CHARGE);
        chargeAction.setAmmount(chargeBankAccountDTO.getAmmount());
        chargeAction.setCurrencyType(chargeBankAccountDTO.getCurrencyType());
        chargeAction.setSource(source);
        
        accountActionRepository.save(chargeAction);
        
        return bankAccountMapper.toDto(source);
    }
    
    /**
     * try transfer among bank accounts
     *
     * @param transferDTO as transfer details
     * @return update bankAccount
     */
    @Override
    public List<BankAccountDTO> tryTransfer(TransferDTO transferDTO) {
        log.debug("Request to transfer among bank accounts : {}", transferDTO);
        Optional<BankAccount> sourceOptional = bankAccountRepository.findByIdForUpdate(transferDTO.getSourceId());
        if(sourceOptional.isPresent() == false)
        	throw new BadRequestAlertException("Bank account doesn't exist to transfer from", ENTITY_NAME, "BankAccount_transfer_sourceId_not_exists");

        BankAccount source = sourceOptional.get();
        
        Double reducedBalance = this.computeReduction(source, transferDTO.getAmmount(), transferDTO.getCurrencyType());
        if( reducedBalance >= 0)
        	source.setBalance(reducedBalance);
        else
        	throw new BadRequestAlertException("Insufficient fund to transfer from source", ENTITY_NAME, "BankAccount_transfer_source_insufficient_fund");
        
        Optional<BankAccount> destinationOptional = bankAccountRepository.findByIdForUpdate(transferDTO.getDestinationId());
        if(destinationOptional.isPresent() == false)
        	throw new BadRequestAlertException("Bank account doesn't exist to transfer to", ENTITY_NAME, "BankAccount_transfer_destinationId_not_exists");
        
        BankAccount destination = destinationOptional.get();
        destination.setBalance(this.computeAddition(destination, transferDTO.getAmmount(),
        		transferDTO.getCurrencyType()));
        
        AccountAction transferAction = new AccountAction();
        transferAction.setActionType(AccountActionType.TRANSFER);
        transferAction.setSource(source);
        transferAction.setDestination(destination);
        transferAction.setAmmount(transferDTO.getAmmount());
        transferAction.setCurrencyType(transferDTO.getCurrencyType());
        
        accountActionRepository.save(transferAction);

        return bankAccountMapper.toDto(bankAccountRepository.saveAll(Arrays.asList(source, destination)));
    }


    /**
     * Get all the bankAccounts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BankAccountDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BankAccounts");
        return bankAccountRepository.findAll(pageable)
            .map(bankAccountMapper::toDto);
    }


    /**
     * Get one bankAccount by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BankAccountDTO> findOne(Long id) {
        log.debug("Request to get BankAccount : {}", id);
        return bankAccountRepository.findById(id)
            .map(bankAccountMapper::toDto);
    }

    /**
     * Delete the bankAccount by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BankAccount : {}", id);
        bankAccountRepository.deleteById(id);
    }
    
    private Double computeReduction(BankAccount ba, Double ammount, CurrencyType reductionCurrency ) {
    	if(ba.getCurrencyType() == reductionCurrency )
    		return ba.getBalance() - ammount;
    	else if(ba.getCurrencyType() == CurrencyType.DOLLAR)
    		return ba.getBalance() - ammount / currencyExchangeService.dollarToRial();
    	else
    		return ba.getBalance() - ammount * currencyExchangeService.dollarToRial();
    }
    
    private Double computeAddition(BankAccount ba, Double ammount, CurrencyType additionCurrency ) {
    	if(ba.getCurrencyType() == additionCurrency )
    		return ba.getBalance() + ammount;
    	else if(ba.getCurrencyType() == CurrencyType.DOLLAR)
    		return ba.getBalance() + ammount / currencyExchangeService.dollarToRial();
    	else
    		return ba.getBalance() + ammount * currencyExchangeService.dollarToRial();
    }
}
