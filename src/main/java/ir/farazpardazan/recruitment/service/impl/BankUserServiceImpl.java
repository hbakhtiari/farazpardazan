package ir.farazpardazan.recruitment.service.impl;

import ir.farazpardazan.recruitment.service.BankUserService;
import ir.farazpardazan.recruitment.domain.BankUser;
import ir.farazpardazan.recruitment.repository.BankUserRepository;
import ir.farazpardazan.recruitment.service.dto.BankUserDTO;
import ir.farazpardazan.recruitment.service.dto.BankUserUpdateDTO;
import ir.farazpardazan.recruitment.service.mapper.BankUserMapper;
import ir.farazpardazan.recruitment.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing BankUser.
 */
@Service
@Transactional
public class BankUserServiceImpl implements BankUserService {

    private final Logger log = LoggerFactory.getLogger(BankUserServiceImpl.class);
    
    private static final String ENTITY_NAME = "recruitmentTestBankUser";

    private final BankUserRepository bankUserRepository;

    private final BankUserMapper bankUserMapper;

    public BankUserServiceImpl(BankUserRepository bankUserRepository, BankUserMapper bankUserMapper) {
        this.bankUserRepository = bankUserRepository;
        this.bankUserMapper = bankUserMapper;
    }

    /**
     * Save a bankUser.
     *
     * @param bankUserDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BankUserDTO save(BankUserDTO bankUserDTO) {
        log.debug("Request to save BankUser : {}", bankUserDTO);

        BankUser bankUser = bankUserMapper.toEntity(bankUserDTO);
        bankUser = bankUserRepository.save(bankUser);
        return bankUserMapper.toDto(bankUser);
    }

    /**
     * Update a bankUser.
     *
     * @param bankUserDTO the entity to update
     * @return the persisted entity
     */
    @Override
    public BankUserDTO update(BankUserUpdateDTO bankUserDTO) {
        log.debug("Request to update a BankUser : {}", bankUserDTO);
        
        Optional<BankUser> bankUserOptional = bankUserRepository.findById(bankUserDTO.getId());
        if(bankUserOptional.isPresent() == false)
        		throw new BadRequestAlertException("No such bank user available", ENTITY_NAME, "bankUser_id_not_found");
        BankUser bankUser = bankUserOptional.get();
        bankUser.setFirstName(bankUserDTO.getFirstName());
        bankUser.setLastName(bankUserDTO.getLastName());
        bankUser = bankUserRepository.save(bankUser);
        return bankUserMapper.toDto(bankUser);
    }

    /**
     * Get all the bankUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BankUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BankUsers");
        return bankUserRepository.findAll(pageable)
            .map(bankUserMapper::toDto);
    }

    /**
     * Get one bankUser by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BankUserDTO> findOne(Long id) {
        log.debug("Request to get BankUser : {}", id);
        return bankUserRepository.findById(id)
            .map(bankUserMapper::toDto);
    }

    /**
     * Delete the bankUser by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BankUser : {}", id);
        bankUserRepository.deleteById(id);
    }
}
