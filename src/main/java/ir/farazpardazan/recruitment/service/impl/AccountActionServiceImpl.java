package ir.farazpardazan.recruitment.service.impl;

import ir.farazpardazan.recruitment.service.AccountActionService;
import ir.farazpardazan.recruitment.domain.AccountAction;
import ir.farazpardazan.recruitment.repository.AccountActionRepository;
import ir.farazpardazan.recruitment.service.dto.AccountActionDTO;
import ir.farazpardazan.recruitment.service.mapper.AccountActionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing AccountAction.
 */
@Service
@Transactional
public class AccountActionServiceImpl implements AccountActionService {

    private final Logger log = LoggerFactory.getLogger(AccountActionServiceImpl.class);

    private final AccountActionRepository accountActionRepository;

    private final AccountActionMapper accountActionMapper;

    public AccountActionServiceImpl(AccountActionRepository accountActionRepository, AccountActionMapper accountActionMapper) {
        this.accountActionRepository = accountActionRepository;
        this.accountActionMapper = accountActionMapper;
    }

    /**
     * Save a accountAction.
     *
     * @param accountActionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AccountActionDTO save(AccountActionDTO accountActionDTO) {
        log.debug("Request to save AccountAction : {}", accountActionDTO);

        AccountAction accountAction = accountActionMapper.toEntity(accountActionDTO);
        accountAction = accountActionRepository.save(accountAction);
        return accountActionMapper.toDto(accountAction);
    }

    /**
     * Get all the accountActions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AccountActionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AccountActions");
        return accountActionRepository.findAll(pageable)
            .map(accountActionMapper::toDto);
    }

    /**
     * Get all the accountActions for a BankAccount if bankAccountId is not null
     *
     * @param pageable the pagination information
     * @param bankAccountId the id of the BankAccount
     * @return the list of AccountActions for a BankAccount, or list of all AccountActions if bankAccountId is null
     */
    @Override
    public Page<AccountActionDTO> findAllForBankAccount(Pageable pageable, Long bankAccountId) {
        log.debug("Request to get all AccountActions for a BankAccount");
        return accountActionRepository.findAllForBankAccount(pageable, bankAccountId)
            .map(accountActionMapper::toDto);
    }

    /**
     * Get one accountAction by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AccountActionDTO> findOne(Long id) {
        log.debug("Request to get AccountAction : {}", id);
        return accountActionRepository.findById(id)
            .map(accountActionMapper::toDto);
    }

    /**
     * Delete the accountAction by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AccountAction : {}", id);
        accountActionRepository.deleteById(id);
    }
}
