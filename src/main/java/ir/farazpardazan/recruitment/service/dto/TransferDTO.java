package ir.farazpardazan.recruitment.service.dto;

import ir.farazpardazan.recruitment.domain.enumeration.CurrencyType;

public class TransferDTO {
	
	private Long sourceId;

	private Long destinationId;

	private CurrencyType currencyType;

	private Double ammount;

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public Long getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(Long destinationId) {
		this.destinationId = destinationId;
	}

	public CurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(CurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public Double getAmmount() {
		return ammount;
	}

	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}

}
