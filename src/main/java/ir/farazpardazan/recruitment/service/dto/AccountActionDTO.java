package ir.farazpardazan.recruitment.service.dto;

import java.io.Serializable;
import java.util.Objects;
import ir.farazpardazan.recruitment.domain.enumeration.AccountActionType;
import ir.farazpardazan.recruitment.domain.enumeration.CurrencyType;

/**
 * A DTO for the AccountAction entity.
 */
public class AccountActionDTO implements Serializable {

    private Long id;

    private AccountActionType actionType;

    private Double ammount;

    private CurrencyType currencyType;

    private Long sourceId;

    private Long destinationId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountActionType getActionType() {
        return actionType;
    }

    public void setActionType(AccountActionType actionType) {
        this.actionType = actionType;
    }

    public Double getAmmount() {
        return ammount;
    }

    public void setAmmount(Double ammount) {
        this.ammount = ammount;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long bankAccountId) {
        this.sourceId = bankAccountId;
    }

    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long bankAccountId) {
        this.destinationId = bankAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountActionDTO accountActionDTO = (AccountActionDTO) o;
        if (accountActionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accountActionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AccountActionDTO{" +
            "id=" + getId() +
            ", actionType='" + getActionType() + "'" +
            ", ammount=" + getAmmount() +
            ", currencyType='" + getCurrencyType() + "'" +
            ", source=" + getSourceId() +
            ", destination=" + getDestinationId() +
            "}";
    }
}
