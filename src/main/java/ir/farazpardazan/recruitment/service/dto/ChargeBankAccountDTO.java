package ir.farazpardazan.recruitment.service.dto;

import ir.farazpardazan.recruitment.domain.enumeration.CurrencyType;

public class ChargeBankAccountDTO {
	
	private Long bankAccountId;
	
	private Double ammount;
	
	private CurrencyType currencyType;

	public Long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	public Double getAmmount() {
		return ammount;
	}

	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}

	public CurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(CurrencyType currencyType) {
		this.currencyType = currencyType;
	}

}
