package ir.farazpardazan.recruitment.service;

import ir.farazpardazan.recruitment.service.dto.AccountActionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing AccountAction.
 */
public interface AccountActionService {

    /**
     * Save a accountAction.
     *
     * @param accountActionDTO the entity to save
     * @return the persisted entity
     */
    AccountActionDTO save(AccountActionDTO accountActionDTO);

    /**
     * Get all the accountActions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AccountActionDTO> findAll(Pageable pageable);
    
    /**
     * Get all the accountActions for a BankAccount if bankAccountId is not null
     *
     * @param pageable the pagination information
     * @param bankAccountId the id of the BankAccount
     * @return the list of AccountActions for a BankAccount, or list of all AccountActions if bankAccountId is null
     */
    Page<AccountActionDTO> findAllForBankAccount(Pageable pageable, Long bankAccountId);

    /**
     * Get the "id" accountAction.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AccountActionDTO> findOne(Long id);

    /**
     * Delete the "id" accountAction.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
