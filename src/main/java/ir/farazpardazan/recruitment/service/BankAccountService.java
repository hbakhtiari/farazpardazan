package ir.farazpardazan.recruitment.service;

import ir.farazpardazan.recruitment.service.dto.BankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.ChargeBankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.PayByBankAccountDTO;
import ir.farazpardazan.recruitment.service.dto.TransferDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing BankAccount.
 */
public interface BankAccountService {

    /**
     * Save a bankAccount.
     *
     * @param bankAccountDTO the entity to save
     * @return the persisted entity
     */
    BankAccountDTO save(BankAccountDTO bankAccountDTO);

    /**
     * Pay by bankAccount.
     *
     * @param payByBankAccountDTO as payment details
     * @return update bankAccount
     */
    BankAccountDTO payByBankAccount(PayByBankAccountDTO payByBankAccountDTO );
    
    /**
     * charge bankAccount.
     *
     * @param chargeBankAccountDTO as charging details
     * @return update bankAccount
     */
    BankAccountDTO chargeBankAccount(ChargeBankAccountDTO chargeBankAccountDTO);
    
    /**
     * try transfer among bank accounts
     *
     * @param transferDTO as transfer details
     * @return update bankAccount
     */
    List<BankAccountDTO> tryTransfer(TransferDTO transferDTO);

    /**
     * Get all the bankAccounts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<BankAccountDTO> findAll(Pageable pageable);


    /**
     * Get the "id" bankAccount.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<BankAccountDTO> findOne(Long id);

    /**
     * Delete the "id" bankAccount.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
