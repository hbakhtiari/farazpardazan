package ir.farazpardazan.recruitment.service;

import ir.farazpardazan.recruitment.service.dto.BankUserDTO;
import ir.farazpardazan.recruitment.service.dto.BankUserUpdateDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing BankUser.
 */
public interface BankUserService {

    /**
     * Save a bankUser.
     *
     * @param bankUserDTO the entity to save
     * @return the persisted entity
     */
    BankUserDTO save(BankUserDTO bankUserDTO);
    
    /**
     * Update a bankUser.
     *
     * @param bankUserDTO the entity to update
     * @return the persisted entity
     */
    BankUserDTO update(BankUserUpdateDTO bankUserDTO);

    /**
     * Get all the bankUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<BankUserDTO> findAll(Pageable pageable);


    /**
     * Get the "id" bankUser.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<BankUserDTO> findOne(Long id);

    /**
     * Delete the "id" bankUser.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
