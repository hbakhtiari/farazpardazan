package ir.farazpardazan.recruitment.web.rest;

import ir.farazpardazan.recruitment.RecruitmentTestApp;

import ir.farazpardazan.recruitment.domain.AccountAction;
import ir.farazpardazan.recruitment.repository.AccountActionRepository;
import ir.farazpardazan.recruitment.service.AccountActionService;
import ir.farazpardazan.recruitment.service.dto.AccountActionDTO;
import ir.farazpardazan.recruitment.service.mapper.AccountActionMapper;
import ir.farazpardazan.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static ir.farazpardazan.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ir.farazpardazan.recruitment.domain.enumeration.AccountActionType;
import ir.farazpardazan.recruitment.domain.enumeration.CurrencyType;
/**
 * Test class for the AccountActionResource REST controller.
 *
 * @see AccountActionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RecruitmentTestApp.class)
public class AccountActionResourceIntTest {

    private static final AccountActionType DEFAULT_ACTION_TYPE = AccountActionType.TRANSFER;
    private static final AccountActionType UPDATED_ACTION_TYPE = AccountActionType.PAYMENT;

    private static final Double DEFAULT_AMMOUNT = 1D;
    private static final Double UPDATED_AMMOUNT = 2D;

    private static final CurrencyType DEFAULT_CURRENCY_TYPE = CurrencyType.DOLLAR;
    private static final CurrencyType UPDATED_CURRENCY_TYPE = CurrencyType.RIAL;

    @Autowired
    private AccountActionRepository accountActionRepository;

    @Autowired
    private AccountActionMapper accountActionMapper;

    @Autowired
    private AccountActionService accountActionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAccountActionMockMvc;

    private AccountAction accountAction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AccountActionResource accountActionResource = new AccountActionResource(accountActionService);
        this.restAccountActionMockMvc = MockMvcBuilders.standaloneSetup(accountActionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountAction createEntity(EntityManager em) {
        AccountAction accountAction = new AccountAction()
            .actionType(DEFAULT_ACTION_TYPE)
            .ammount(DEFAULT_AMMOUNT)
            .currencyType(DEFAULT_CURRENCY_TYPE);
        return accountAction;
    }

    @Before
    public void initTest() {
        accountAction = createEntity(em);
    }

    @Test
    @Transactional
    public void createAccountAction() throws Exception {
        int databaseSizeBeforeCreate = accountActionRepository.findAll().size();

        // Create the AccountAction
        AccountActionDTO accountActionDTO = accountActionMapper.toDto(accountAction);
        restAccountActionMockMvc.perform(post("/api/account-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountActionDTO)))
            .andExpect(status().isCreated());

        // Validate the AccountAction in the database
        List<AccountAction> accountActionList = accountActionRepository.findAll();
        assertThat(accountActionList).hasSize(databaseSizeBeforeCreate + 1);
        AccountAction testAccountAction = accountActionList.get(accountActionList.size() - 1);
        assertThat(testAccountAction.getActionType()).isEqualTo(DEFAULT_ACTION_TYPE);
        assertThat(testAccountAction.getAmmount()).isEqualTo(DEFAULT_AMMOUNT);
        assertThat(testAccountAction.getCurrencyType()).isEqualTo(DEFAULT_CURRENCY_TYPE);
    }

    @Test
    @Transactional
    public void createAccountActionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accountActionRepository.findAll().size();

        // Create the AccountAction with an existing ID
        accountAction.setId(1L);
        AccountActionDTO accountActionDTO = accountActionMapper.toDto(accountAction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccountActionMockMvc.perform(post("/api/account-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountActionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AccountAction in the database
        List<AccountAction> accountActionList = accountActionRepository.findAll();
        assertThat(accountActionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAccountActions() throws Exception {
        // Initialize the database
        accountActionRepository.saveAndFlush(accountAction);

        // Get all the accountActionList
        restAccountActionMockMvc.perform(get("/api/account-actions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accountAction.getId().intValue())))
            .andExpect(jsonPath("$.[*].actionType").value(hasItem(DEFAULT_ACTION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].ammount").value(hasItem(DEFAULT_AMMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].currencyType").value(hasItem(DEFAULT_CURRENCY_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getAccountAction() throws Exception {
        // Initialize the database
        accountActionRepository.saveAndFlush(accountAction);

        // Get the accountAction
        restAccountActionMockMvc.perform(get("/api/account-actions/{id}", accountAction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(accountAction.getId().intValue()))
            .andExpect(jsonPath("$.actionType").value(DEFAULT_ACTION_TYPE.toString()))
            .andExpect(jsonPath("$.ammount").value(DEFAULT_AMMOUNT.doubleValue()))
            .andExpect(jsonPath("$.currencyType").value(DEFAULT_CURRENCY_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAccountAction() throws Exception {
        // Get the accountAction
        restAccountActionMockMvc.perform(get("/api/account-actions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccountAction() throws Exception {
        // Initialize the database
        accountActionRepository.saveAndFlush(accountAction);

        int databaseSizeBeforeUpdate = accountActionRepository.findAll().size();

        // Update the accountAction
        AccountAction updatedAccountAction = accountActionRepository.findById(accountAction.getId()).get();
        // Disconnect from session so that the updates on updatedAccountAction are not directly saved in db
        em.detach(updatedAccountAction);
        updatedAccountAction
            .actionType(UPDATED_ACTION_TYPE)
            .ammount(UPDATED_AMMOUNT)
            .currencyType(UPDATED_CURRENCY_TYPE);
        AccountActionDTO accountActionDTO = accountActionMapper.toDto(updatedAccountAction);

        restAccountActionMockMvc.perform(put("/api/account-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountActionDTO)))
            .andExpect(status().isOk());

        // Validate the AccountAction in the database
        List<AccountAction> accountActionList = accountActionRepository.findAll();
        assertThat(accountActionList).hasSize(databaseSizeBeforeUpdate);
        AccountAction testAccountAction = accountActionList.get(accountActionList.size() - 1);
        assertThat(testAccountAction.getActionType()).isEqualTo(UPDATED_ACTION_TYPE);
        assertThat(testAccountAction.getAmmount()).isEqualTo(UPDATED_AMMOUNT);
        assertThat(testAccountAction.getCurrencyType()).isEqualTo(UPDATED_CURRENCY_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingAccountAction() throws Exception {
        int databaseSizeBeforeUpdate = accountActionRepository.findAll().size();

        // Create the AccountAction
        AccountActionDTO accountActionDTO = accountActionMapper.toDto(accountAction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccountActionMockMvc.perform(put("/api/account-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accountActionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AccountAction in the database
        List<AccountAction> accountActionList = accountActionRepository.findAll();
        assertThat(accountActionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAccountAction() throws Exception {
        // Initialize the database
        accountActionRepository.saveAndFlush(accountAction);

        int databaseSizeBeforeDelete = accountActionRepository.findAll().size();

        // Get the accountAction
        restAccountActionMockMvc.perform(delete("/api/account-actions/{id}", accountAction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AccountAction> accountActionList = accountActionRepository.findAll();
        assertThat(accountActionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountAction.class);
        AccountAction accountAction1 = new AccountAction();
        accountAction1.setId(1L);
        AccountAction accountAction2 = new AccountAction();
        accountAction2.setId(accountAction1.getId());
        assertThat(accountAction1).isEqualTo(accountAction2);
        accountAction2.setId(2L);
        assertThat(accountAction1).isNotEqualTo(accountAction2);
        accountAction1.setId(null);
        assertThat(accountAction1).isNotEqualTo(accountAction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountActionDTO.class);
        AccountActionDTO accountActionDTO1 = new AccountActionDTO();
        accountActionDTO1.setId(1L);
        AccountActionDTO accountActionDTO2 = new AccountActionDTO();
        assertThat(accountActionDTO1).isNotEqualTo(accountActionDTO2);
        accountActionDTO2.setId(accountActionDTO1.getId());
        assertThat(accountActionDTO1).isEqualTo(accountActionDTO2);
        accountActionDTO2.setId(2L);
        assertThat(accountActionDTO1).isNotEqualTo(accountActionDTO2);
        accountActionDTO1.setId(null);
        assertThat(accountActionDTO1).isNotEqualTo(accountActionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(accountActionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(accountActionMapper.fromId(null)).isNull();
    }
}
