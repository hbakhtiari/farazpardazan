package ir.farazpardazan.recruitment.web.rest;

import ir.farazpardazan.recruitment.RecruitmentTestApp;

import ir.farazpardazan.recruitment.domain.BankUser;
import ir.farazpardazan.recruitment.repository.BankUserRepository;
import ir.farazpardazan.recruitment.service.BankUserService;
import ir.farazpardazan.recruitment.service.dto.BankUserDTO;
import ir.farazpardazan.recruitment.service.mapper.BankUserMapper;
import ir.farazpardazan.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static ir.farazpardazan.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BankUserResource REST controller.
 *
 * @see BankUserResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RecruitmentTestApp.class)
public class BankUserResourceIntTest {

    private static final String DEFAULT_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_NATIONAL_CODE = "BBBBBBBBBB";

    @Autowired
    private BankUserRepository bankUserRepository;

    @Autowired
    private BankUserMapper bankUserMapper;

    @Autowired
    private BankUserService bankUserService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBankUserMockMvc;

    private BankUser bankUser;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BankUserResource bankUserResource = new BankUserResource(bankUserService);
        this.restBankUserMockMvc = MockMvcBuilders.standaloneSetup(bankUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BankUser createEntity(EntityManager em) {
        BankUser bankUser = new BankUser()
            .login(DEFAULT_LOGIN)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .nationalCode(DEFAULT_NATIONAL_CODE);
        return bankUser;
    }

    @Before
    public void initTest() {
        bankUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createBankUser() throws Exception {
        int databaseSizeBeforeCreate = bankUserRepository.findAll().size();

        // Create the BankUser
        BankUserDTO bankUserDTO = bankUserMapper.toDto(bankUser);
        restBankUserMockMvc.perform(post("/api/bank-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankUserDTO)))
            .andExpect(status().isCreated());

        // Validate the BankUser in the database
        List<BankUser> bankUserList = bankUserRepository.findAll();
        assertThat(bankUserList).hasSize(databaseSizeBeforeCreate + 1);
        BankUser testBankUser = bankUserList.get(bankUserList.size() - 1);
        assertThat(testBankUser.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(testBankUser.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testBankUser.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testBankUser.getNationalCode()).isEqualTo(DEFAULT_NATIONAL_CODE);
    }

    @Test
    @Transactional
    public void createBankUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bankUserRepository.findAll().size();

        // Create the BankUser with an existing ID
        bankUser.setId(1L);
        BankUserDTO bankUserDTO = bankUserMapper.toDto(bankUser);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBankUserMockMvc.perform(post("/api/bank-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankUser in the database
        List<BankUser> bankUserList = bankUserRepository.findAll();
        assertThat(bankUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLoginIsRequired() throws Exception {
        int databaseSizeBeforeTest = bankUserRepository.findAll().size();
        // set the field null
        bankUser.setLogin(null);

        // Create the BankUser, which fails.
        BankUserDTO bankUserDTO = bankUserMapper.toDto(bankUser);

        restBankUserMockMvc.perform(post("/api/bank-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankUserDTO)))
            .andExpect(status().isBadRequest());

        List<BankUser> bankUserList = bankUserRepository.findAll();
        assertThat(bankUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBankUsers() throws Exception {
        // Initialize the database
        bankUserRepository.saveAndFlush(bankUser);

        // Get all the bankUserList
        restBankUserMockMvc.perform(get("/api/bank-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bankUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].nationalCode").value(hasItem(DEFAULT_NATIONAL_CODE.toString())));
    }
    
    @Test
    @Transactional
    public void getBankUser() throws Exception {
        // Initialize the database
        bankUserRepository.saveAndFlush(bankUser);

        // Get the bankUser
        restBankUserMockMvc.perform(get("/api/bank-users/{id}", bankUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bankUser.getId().intValue()))
            .andExpect(jsonPath("$.login").value(DEFAULT_LOGIN.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.nationalCode").value(DEFAULT_NATIONAL_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBankUser() throws Exception {
        // Get the bankUser
        restBankUserMockMvc.perform(get("/api/bank-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBankUser() throws Exception {
        // Initialize the database
        bankUserRepository.saveAndFlush(bankUser);

        int databaseSizeBeforeUpdate = bankUserRepository.findAll().size();

        // Update the bankUser
        BankUser updatedBankUser = bankUserRepository.findById(bankUser.getId()).get();
        // Disconnect from session so that the updates on updatedBankUser are not directly saved in db
        em.detach(updatedBankUser);
        updatedBankUser
            .login(UPDATED_LOGIN)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .nationalCode(UPDATED_NATIONAL_CODE);
        BankUserDTO bankUserDTO = bankUserMapper.toDto(updatedBankUser);

        restBankUserMockMvc.perform(put("/api/bank-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankUserDTO)))
            .andExpect(status().isOk());

        // Validate the BankUser in the database
        List<BankUser> bankUserList = bankUserRepository.findAll();
        assertThat(bankUserList).hasSize(databaseSizeBeforeUpdate);
        BankUser testBankUser = bankUserList.get(bankUserList.size() - 1);
        assertThat(testBankUser.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testBankUser.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testBankUser.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testBankUser.getNationalCode()).isEqualTo(UPDATED_NATIONAL_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingBankUser() throws Exception {
        int databaseSizeBeforeUpdate = bankUserRepository.findAll().size();

        // Create the BankUser
        BankUserDTO bankUserDTO = bankUserMapper.toDto(bankUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBankUserMockMvc.perform(put("/api/bank-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bankUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BankUser in the database
        List<BankUser> bankUserList = bankUserRepository.findAll();
        assertThat(bankUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBankUser() throws Exception {
        // Initialize the database
        bankUserRepository.saveAndFlush(bankUser);

        int databaseSizeBeforeDelete = bankUserRepository.findAll().size();

        // Get the bankUser
        restBankUserMockMvc.perform(delete("/api/bank-users/{id}", bankUser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BankUser> bankUserList = bankUserRepository.findAll();
        assertThat(bankUserList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankUser.class);
        BankUser bankUser1 = new BankUser();
        bankUser1.setId(1L);
        BankUser bankUser2 = new BankUser();
        bankUser2.setId(bankUser1.getId());
        assertThat(bankUser1).isEqualTo(bankUser2);
        bankUser2.setId(2L);
        assertThat(bankUser1).isNotEqualTo(bankUser2);
        bankUser1.setId(null);
        assertThat(bankUser1).isNotEqualTo(bankUser2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BankUserDTO.class);
        BankUserDTO bankUserDTO1 = new BankUserDTO();
        bankUserDTO1.setId(1L);
        BankUserDTO bankUserDTO2 = new BankUserDTO();
        assertThat(bankUserDTO1).isNotEqualTo(bankUserDTO2);
        bankUserDTO2.setId(bankUserDTO1.getId());
        assertThat(bankUserDTO1).isEqualTo(bankUserDTO2);
        bankUserDTO2.setId(2L);
        assertThat(bankUserDTO1).isNotEqualTo(bankUserDTO2);
        bankUserDTO1.setId(null);
        assertThat(bankUserDTO1).isNotEqualTo(bankUserDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bankUserMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bankUserMapper.fromId(null)).isNull();
    }
}
